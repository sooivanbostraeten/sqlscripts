USE ModernWays;
SELECT Voornaam, Familienaam, Titel
FROM Boeken
-- <> Betekent niet gelijk aan (of omgekeerde van =)
WHERE Titel<>NULL;

-- Booleaanse expressies:
SELECT TRUE OR NULL;
SELECT FALSE OR NULL;
SELECT NULL OR NULL;
SELECT NOT NULL;
SELECT TRUE XOR NULL;
SELECT FALSE XOR NULL;
SELECT NULL XOR NULL;
SELECT TRUE AND NULL;
SELECT FALSE AND NULL;
SELECT NULL AND NULL;

