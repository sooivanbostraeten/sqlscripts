USE ModernWays;
-- oplopend sorteren volgens familienaam
-- ascending
SELECT Voornaam, Familienaam, Titel FROM Boeken
ORDER BY Familienaam ASC, Voornaam, Titel;
-- aflopend sorteren volgens familienaam
-- descending
SELECT Voornaam, Familienaam, Titel FROM Boeken
ORDER BY Familienaam DESC, Voornaam, Titel;

SHOW FULL COLUMNS FROM Boeken;