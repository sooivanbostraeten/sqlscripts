USE ModernWays;
INSERT INTO Boeken(
Voornaam,
Familienaam,
Titel,
Stad,
Uitgeverij,
Verschijningsjaar,
Herdruk,
Commentaar,
Categorie)
VALUES(
   'Emile',
   'Bréhier',
   'Cours de Philosophie',
   'Paris',
   'Gallimard',
   '1935',
   '1960',
   'Goed boek',
   'Filosofie'
),
(
   'Andre',
   'Breton',
   'Nadja',
   'Paris',
   'NRF',
   '1928',
   '?',
   'Nog te lezen',
   'Roman');
SELECT Voornaam, Familienaam, Titel FROM Boeken WHERE Familienaam<='Breton';

-- Collation bekijken voor de tabel Boeken: ai=accent insensitive, ci = capital insensitive
SHOW FULL COLUMNS FROM Boeken;

USE ModernWays;
-- Andere Collation voor WHERE Clauseule, as= accent sensitive; cs = capital sensitive
-- Met deze regel ga je enkel boek van Breton zien en niet breton of bréton
SELECT *FROM Boeken WHERE Familienaam COLLATE utf8mb4_0900_as_cs='Breton';

-- speciaal geval: IS NULL
SELECT *FROM Boeken WHERE Categorie IS NULL;


