USE ModernWays;
-- begint met kleine letter b (volgens collation)
SELECT Voornaam, Familienaam, Titel FROM Boeken WHERE Familienaam LIKE 'b%';
-- begint met hoofdletter B (volgens collation)
SELECT Voornaam, Familienaam, Titel FROM Boeken WHERE Familienaam LIKE 'B%';
-- eindig op letter s
SELECT Voornaam, Familienaam, Titel FROM Boeken WHERE Familienaam LIKE '%s';

USE ModernWays;
SELECT Voornaam, Familienaam, Verschijningsjaar FROM Boeken WHERE Titel LIKE '%economie%';
