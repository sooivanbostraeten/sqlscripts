USE ModernWays;
SET SQL_SAFE_UPDATES=0;
UPDATE Boeken SET Categorie='MetaFysica';
SET SQL_SAFE_UPDATES=1;
SELECT *FROM Boeken;

USE ModernWays;
UPDATE Boeken SET Categorie='Wetenschap',Titel='Een boek';

-- Verfijnd aanpassen:
USE ModernWays;
SET SQL_SAFE_UPDATES=0;
UPDATE boeken SET Categorie='Wiskundige logica. Mathetmatische Logica' WHERE Titel='Logicaboek';
SET SQL_SAFE_UPDATES=1;
SELECT *FROM BOEKEN ORDER BY Titel;

USE ModernWays;
-- filteren op hoofdlettergevoelige waardes (cs of capital sensitive) en accentgevoelig (as of accent sensitive)
SELECT Voornaam, Familienaam, Titel
FROM Boeken
WHERE Titel COLLATE utf8mb4_0900_as_cs='Logicaboek';

-- Samengestelde constructies:
-- OR logische operator (OR = oftwel)
USE ModernWays;
SET SQL_SAFE_UPDATES=0;
UPDATE Boeken
SET Categorie='Geschiedenis' WHERE (Familienaam='Braudel' OR Familienaam='Bernard' OR Familienaam='Bloch');
SET SQL_SAFE_UPDATES=1;
SELECT * FROM Boeken; 

-- meerdere kolommen tegelijk wijzigen:
USE ModernWays;
SET SQL_SAFE_UPDATES=0;
UPDATE Boeken
SET Voornaam='Geert', Familienaam='Hoste', Uitgeverij='De Bezige Olifant' WHERE Titel='De stad van God';
SET SQL_SAFE_UPDATES=1;
SELECT *FROM Boeken;

-- gebruik van expressies/functies in UPDATE bepaalde veldnamen
USE ModernWays;
SET SQL_SAFE_UPDATES=0;
UPDATE Boeken SET Categorie=concat('[1]',Categorie) WHERE Categorie='Wiskunde';
SET SQL_SAFE_UPDATES=1;
SELECT *FROM Boeken ORDER BY Categorie;

-- Hier heb je '[1] gezet voor de tekst als er 'Wiskunde' staat --> [1]Wiskunde
-- nu halen we dat weer weg
-- left(x,y) is zoals substring(x,1,y)
USE ModernWays;
SET SQL_SAFE_UPDATES=0;
-- Weglaten in kolom met veldnaam 'Categorie', vanaf 4e letter, aantal letters van waarde -3 (want dat zijn eerste 3 letters '[1]')
-- WAAR 1 linkse 3 letters overeenkomen met [1]
UPDATE Boeken SET Categorie= substring(Categorie,4,length(Categorie)-3) WHERE left(Categorie,3)='[1]';
SET SQL_SAFE_UPDATES=1;
SELECT *FROM Boeken ORDER BY Categorie;



