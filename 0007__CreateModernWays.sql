CREATE DATABASE IF NOT EXISTS ModernWays;
USE ModernWays;
CREATE TABLE IF NOT EXISTS MijnBoeken(
Voornaam VARCHAR(50) CHAR SET utf8mb4,
Familienaam VARCHAR(200) CHAR SET utf8mb4 NOT NULL,
Titel VARCHAR(255) CHAR SET utf8mb4,
Stad VARCHAR(50) CHAR SET utf8mb4,
Verschijningsjaar VARCHAR(4),
Uitgeverij VARCHAR(80) CHAR SET utf8mb4,
Herdruk VARCHAR(4),
Commentaar VARCHAR(150) CHAR SET utf8mb4
);


