USE ModernWays;
SELECT *FROM Boeken;
USE ModernWays;
SELECT Voornaam, Familienaam FROM Boeken;

USE ModernWays;
SELECT Boeken.Voornaam, Boeken.Familienaam FROM Boeken;

-- Data ordenen:
USE ModernWays;
SELECT *FROM Boeken ORDER BY Familienaam;

USE ModernWays;
SELECT *FROM Boeken ORDER BY Familienaam, Voornaam, Titel;

-- Data verwerken met functies:
USE ModernWays;
-- om alleen de initialen te tonen, we starten met het eerste karakter en we nemen 1 karakter.
-- Begin niet met 0, maar met 1. Dit is anders dan veel programmeertalen
SELECT Titel, substring(Voornaam,1,1), substring(Familienaam,1,1)
FROM Boeken ORDER BY Familienaam, Titel;

USE ModernWays;
-- Om de initialen te tonen met een punt achteraan gebruiken we de functie "string concatenation": 
-- het aan elkaar plakken van strings
SELECT Titel, concat(substring(Voornaam,1,1),'.',substring(Familienaam,1,1),'.')
FROM Boeken ORDER BY Titel, Voornaam;

use ModernWays;
SELECT titel, concat(substring(Voornaam,1,1),'.',substring(Familienaam,1,1),'.')
AS Initialen FROM Boeken ORDER BY Titel, Voornaam;

