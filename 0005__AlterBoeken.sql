USE ModernWays;
-- Beperkingen toevoegen: Familienaam moet ingevuld worden
ALTER TABLE Boeken CHANGE Familienaam Familienaam VARCHAR(200) CHAR SET utf8mb4 NOT NULL;
-- Tabelnamen wijzigen
USE ModernWays;
RENAME TABLE `Boeken` TO `MijnBoeken`;

